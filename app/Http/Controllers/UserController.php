<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return "LISTA DE USUARIOS";
        //NOTA: En laravel las funciones incorporadas
        //se denominan "helpers", view es una de ella.
        $users = array('Javi', 'Ana', 'Diego', 'Sandra');

        // $users = array();

        return view('user.index', ['users' => $users]);
        //busca el fichero (uno de los dos):
        // /resources/views/user/index.php
        // /resources/views/user/index.blade.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "Formulario de alta";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // $otro = "<script>alert('hola mundo')</script>";
        $otro = "Otra variable....";
        $otro = "<h3>Otra variable....</h3>";
        return view('user.show', [
            'id' => $id,
            'otro' => $otro
         ]);

        // $otro = "Otra variable....";
        // // dd(compact('id', 'otro'));
        // return view('user.show', compact('id', 'otro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function especial()
    {
        return redirect('/users');
        // return "Especial";
    }
}
